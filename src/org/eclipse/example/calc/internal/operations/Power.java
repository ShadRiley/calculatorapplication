/*******************************************************************************
 * Copyright (C) 2010, Matthias Sohn <matthias.sohn@sap.com>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;

/**
 * Binary power operation
 */
public class Power extends AbstractOperation implements BinaryOperation {

	/*
	 * Power operation completed in loop below
	 * exponent variable created to set iteration count
	 */
	@Override
	public float perform(float arg1, float arg2) {
		float power = 1;
		float exponent = arg2;
		int i;
		for(i = 1; i <= exponent; i++)
		{
			power = power * arg1;
		}
		return power;
	}

	@Override
	public String getName() {
		return "^";
	}

}

/*******************************************************************************
 * Copyright (C) 2010, Matthias Sohn <matthias.sohn@sap.com>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;

/**
 * Binary power operation
 */
public class Sqrt extends AbstractOperation implements BinaryOperation {

	/*
	 * Power operation completed in loop below
	 * exponent variable created to set iteration count
	 */
	@Override
	public float perform(float arg1, float arg2) {
		float sqrt = arg2 / 2;
		float temp;
		
		do {
			temp = sqrt;
			sqrt = (temp + (arg2 / temp)) / 2;
		} while ((temp - sqrt) != 0);
		
		return sqrt;
	}

	@Override
	public String getName() {
		return "sqrt";
	}

}
